from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from os import environ
from sys import setswitchinterval
from time import sleep

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy import create_engine as sa_create_engine
from sqlalchemy import Column, Identity, Integer, String, Boolean
from sqlalchemy import DateTime

from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from datetime import datetime
from uuid import uuid4

from base64 import b64encode
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP
from ssl import create_default_context as ssl_create_context

from gevent import spawn as gevent_spawn
from gevent.queue import Queue as gQueue
from gevent.threadpool import ThreadPool

from traceback import format_exc

import logging
import pytz


# Basic logging config
log_format = (
    '[%(asctime)s] [%(process)d] [%(levelname)s] '
    '[%(filename)s:%(lineno)d - %(funcName)s] '
    '%(message)s'
)
log_date_format = '%Y-%m-%d %H:%M:%S %z'

# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# Queues and worker pools for processing emails asynchronously
mail_pool_size = 4

mail_queue = gQueue()
mail_worker_pool = ThreadPool(mail_pool_size)


def mail_queue_processor():
    while True:
        healthData = mail_queue.get()

        mail_worker_pool.spawn(mail_response, healthData)
        # Under gevent, keep this loop from being too tight.
        sleep(0)


gevent_spawn(mail_queue_processor)

# Time formatting for emails
time_format = '%I:%M:%S %p %Z, %A %m-%d-%Y'
local_timezone = pytz.timezone('US/Eastern')

# Base logging
base_logger = 'new_hire_logger'

base_log_handler = logging.StreamHandler()
base_log_formatter = logging.Formatter(fmt=log_format, datefmt=log_date_format)
base_log_handler.setFormatter(base_log_formatter)

base_log = logging.getLogger(base_logger)
base_log.setLevel(logging.INFO)
base_log.addHandler(base_log_handler)
base_log.propagate = False

# SMTP config
smtp_server = 'smtp.duke.edu'
smtp_port = 25
smtp_sender = 'Duke EOHW <eohremote@dm.duke.edu>'
smtp_retry_global_count = 5
smtp_retry_global_sleep = 5

# SMTP logging
smtp_logger = 'new_hire_smtp_logger'

smtp_log_file = '/var/log/new_hire_doc_service/smtp_log'
smtp_log_handler = logging.FileHandler(smtp_log_file)
smtp_log_formatter = logging.Formatter(fmt=log_format, datefmt=log_date_format)
smtp_log_handler.setFormatter(smtp_log_formatter)

smtp_log = logging.getLogger(smtp_logger)
smtp_log.setLevel(logging.INFO)
smtp_log.addHandler(smtp_log_handler)
smtp_log.propagate = False

# Health Requirements PDF to attach
attachments_path = '/opt/new_hire_doc_service/attachments/'
reqs_filepath = f'{attachments_path}reqs.pdf'

reqs_file = None
with open(reqs_filepath, 'rb') as f:
    reqs_file = f.read()


class HealthDataField:
    def __init__(self, name, value=''):
        self.name = name
        self.value = value


class HealthData:
    # Class constants for JSON parsing.
    # This prevents spelling/capitalization errors
    # from becoming logic errors.
    COMPLETE = 'complete'
    INCOMPLETE = 'incomplete'
    NO = 'no'
    DOC_MISSING = 'documentation missing information'
    PREVIOUSLY_SUBMITTED = 'previously submitted'
    NOT_REQUIRED = 'not required'
    OTHER = 'other'
    LABEL = 'label'

    field_list = [
        'health_status_form_status',
        'OSHA_respirator_form_status',
        'drug_screen_status',
        'color_vision_screening_status',
        'measles_immunity_status',
        'mumps_immunity_status',
        'rubella_immunity_status',
        'varicella_immunity_status',
        'tdap_immunity_status',
        'flu_immunity_status',
        'covid_immunity_status',
        'tb_status',
        'hepb_immunity_status',
        'polio_immunity_status',
        'MMR_doc1_accepted',
        'MMR_doc1_reject_reason',
        'MMR_doc2_accepted',
        'MMR_doc2_reject_reason',
        'MMR_doc3_accepted',
        'MMR_doc3_reject_reason',
        'MMR_doc4_accepted',
        'MMR_doc4_reject_reason',
        'measles_doc1_accepted',
        'measles_doc1_reject_reason',
        'measles_doc2_accepted',
        'measles_doc2_reject_reason',
        'measles_titer_accepted',
        'measles_titer_reject_reason',
        'measles_incomplete_reason',
        'mumps_doc1_accepted',
        'mumps_doc1_reject_reason',
        'mumps_doc2_accepted',
        'mumps_doc2_reject_reason',
        'mumps_titer_accepted',
        'mumps_titer_reject_reason',
        'mumps_incomplete_reason',
        'rubella_doc1_accepted',
        'rubella_doc1_reject_reason',
        'rubella_titer_accepted',
        'rubella_titer_reject_reason',
        'rubella_incomplete_reason',
        'varicella_doc1_accepted',
        'varicella_doc1_rejected_reason',
        'varicella_doc2_accepted',
        'varicella_doc2_rejected_reason',
        'varicella_doc3_accepted',
        'varicella_doc3_rejected_reason',
        'varicella_titer_accepted',
        'varicella_titer_rejected_reason',
        'varicella_incomplete_reason',
        'tdap_doc_accepted',
        'tdap_doc_rejected_reason',
        'tdap_incomplete_reason',
        'flu_doc_accepted',
        'flu_doc_rejected_reason',
        'flu_incomplete_reason',
        'covid_doc1_accepted',
        'covid_doc1_rejected_reason',
        'covid_doc2_accepted',
        'covid_doc2_rejected_reason',
        'covid_doc3_accepted',
        'covid_doc3_rejected_reason',
        'covid_doc4_accepted',
        'covid_doc4_rejected_reason',
        'covid_doc5_accepted',
        'covid_doc5_rejected_reason',
        'covid_doc6_accepted',
        'covid_doc6_rejected_reason',
        'covid_doc7_accepted',
        'covid_doc7_rejected_reason',
        'covid_incomplete_reason',
        'tb_pos_test_doc_accepted',
        'tb_pos_test_doc_rejected_reason',
        'tb_neg_skin_doc_accepted',
        'tb_neg_skin_doc_rejected_reason',
        'tb_neg_blood_doc_accepted',
        'tb_neg_blood_doc_rejected_reason',
        'chest_xray_1_accepted',
        'chest_xray_1_rejected_reason',
        'chest_xray_2_accepted',
        'chest_xray_2_rejected_reason',
        'tb_next_steps',
        'hepb_doc1_accepted',
        'hepb_doc1_rejected_reason',
        'hepb_doc2_accepted',
        'hepb_doc2_rejected_reason',
        'hepb_doc3_accepted',
        'hepb_doc3_rejected_reason',
        'hepb_doc4_accepted',
        'hepb_doc4_rejected_reason',
        'hepb_doc5_accepted',
        'hepb_doc5_rejected_reason',
        'hepb_doc6_accepted',
        'hepb_doc6_rejected_reason',
        'hepb_titer_accepted',
        'hepb_titer_rejected_reason',
        'hepb_antigen_accepted',
        'hepb_antigen_rejected_reason',
        'hepb_incomplete_reason',
        'polio_doc1_accepted',
        'polio_doc1_rejected_reason',
        'polio_doc2_accepted',
        'polio_doc2_rejected_reason',
        'polio_doc3_accepted',
        'polio_doc3_rejected_reason',
        'polio_doc4_accepted',
        'polio_doc4_rejected_reason',
        'polio_doc5_accepted',
        'polio_doc5_rejected_reason',
        'polio_incomplete_reason',
    ]

    missing_field_list = [
        'mmr_doc1_missing_info',
        'mmr_doc2_missing_info',
        'mmr_doc3_missing_info',
        'mmr_doc4_missing_info',
        'measles_doc1_missing_info',
        'measles_doc2_missing_info',
        'measles_titer_missing_info',
        'mumps_doc1_missing_info',
        'mumps_doc2_missing_info',
        'mumps_titer_missing_info',
        'rubella_doc1_missing_info',
        'rubella_titer_missing_info',
        'varicella_doc1_missing_info',
        'varicella_doc2_missing_info',
        'varicella_doc3_missing_info',
        'varicella_titer_missing_info',
        'tdap_doc_missing_info',
        'flu_doc_missing_info',
        'covid_doc1_missing_info',
        'covid_doc2_missing_info',
        'covid_doc3_missing_info',
        'covid_doc4_missing_info',
        'covid_doc5_missing_info',
        'covid_doc6_missing_info',
        'covid_doc7_missing_info',
        'tb_pos_test_doc_missing_info',
        'tb_neg_skin_doc_missing_info',
        'tb_neg_blood_doc_missing_info',
        'chest_xray_1_missing_info',
        'chest_xray_2_missing_info',
        'hepb_doc1_missing_info',
        'hepb_doc2_missing_info',
        'hepb_doc3_missing_info',
        'hepb_doc4_missing_info',
        'hepb_doc5_missing_info',
        'hepb_doc6_missing_info',
        'hepb_titer_missing_info',
        'hepb_antigen_missing_info',
        'polio_doc1_missing_info',
        'polio_doc2_missing_info',
        'polio_doc3_missing_info',
        'polio_doc4_missing_info',
        'polio_doc5_missing_info',
    ]

    other_field_list = [
        'mmr_doc1_missing_other',
        'mmr_doc2_missing_other',
        'mmr_doc3_missing_other',
        'mmr_doc4_missing_other',
        'measles_doc1_missing_other',
        'measles_doc2_missing_other',
        'measles_titer_missing_other',
        'measles_incomplete_reason_other',
        'mumps_doc1_missing_other',
        'mumps_doc2_missing_other',
        'mumps_titer_missing_other',
        'mumps_incomplete_reason_other',
        'rubella_doc1_missing_other',
        'rubella_titer_missing_other',
        'rubella_incomplete_reason_other',
        'varicella_doc1_missing_other',
        'varicella_doc2_missing_other',
        'varicella_doc3_missing_other',
        'varicella_titer_missing_other',
        'varicella_incomplete_reason_other',
        'tdap_doc_missing_other',
        'tdap_incomplete_reason_other',
        'flu_doc_missing_other',
        'flu_incomplete_reason_other',
        'covid_doc1_missing_other',
        'covid_doc2_missing_other',
        'covid_doc3_missing_other',
        'covid_doc4_missing_other',
        'covid_doc5_missing_other',
        'covid_doc6_missing_other',
        'covid_doc7_missing_other',
        'covid_incomplete_reason_other',
        'tb_pos_test_doc_missing_other',
        'tb_neg_skin_doc_missing_other',
        'tb_neg_blood_doc_missing_other',
        'chest_xray_1_missing_other',
        'chest_xray_2_missing_other',
        'tb_next_steps_other',
        'hepb_doc1_missing_other',
        'hepb_doc2_missing_other',
        'hepb_doc3_missing_other',
        'hepb_doc4_missing_other',
        'hepb_doc5_missing_other',
        'hepb_doc6_missing_other',
        'hepb_titer_missing_other',
        'hepb_antigen_missing_other',
        'hepb_incomplete_reason_other',
        'polio_doc1_missing_other',
        'polio_doc2_missing_other',
        'polio_doc3_missing_other',
        'polio_doc4_missing_other',
        'polio_doc5_missing_other',
        'polio_incomplete_reason_other'
    ]

    def __init__(self, source_json):
        self.duid = HealthDataField('duid')
        try:
            self.duid.value = source_json['meta']['submittedBy']['schoolId']
        except Exception:
            missing_field_msg = (
                'DUID missing from JSON provided by Kuali!'
            )
            base_log.warning(missing_field_msg)

        if not self.duid.value:
            # Bail out; there's no point in doing any more work.
            return

        self.displayName = HealthDataField('displayName')
        try:
            self.displayName.value = (
                source_json['meta']['submittedBy'][self.displayName.name]
            )
        except Exception:
            missing_field_msg = (
                f'{self.displayName.name} missing from JSON provided by Kuali '
                f'for {self.duid.value}'
            )
            base_log.warning(missing_field_msg)

        self.email = HealthDataField('email_address')
        try:
            self.email.value = source_json[self.email.name]
        except Exception:
            missing_field_msg = (
                f'{self.email.name} missing from JSON provided by Kuali '
                f'for {self.duid.value}'
            )
            base_log.warning(missing_field_msg)

        for i in self.field_list:
            setattr(self, i, HealthDataField(i))
            field = getattr(self, i)
            json_entry = source_json.get(i)
            if json_entry:
                try:
                    value = json_entry[self.LABEL]
                    field.value = value
                except Exception:
                    missing_label_msg = (
                        f'{i} missing expected label field '
                        f'for {self.duid.value}'
                    )
                    base_log.info(missing_label_msg)
            else:
                missing_field_msg = (
                    f'{i} missing from JSON provided by Kuali '
                    f'for {self.duid.value}'
                )
                base_log.info(missing_field_msg)

        for i in self.missing_field_list:
            setattr(self, i, HealthDataField(i))
            field = getattr(self, i)
            value_list = []
            value = ''
            json_entry = source_json.get(i)
            if json_entry:
                for item in json_entry:
                    try:
                        value_list.append(item[self.LABEL])
                    except Exception:
                        missing_label_msg = (
                            f'{i} missing expected label field '
                            f'for {self.duid.value}'
                        )
                        base_log.info(missing_label_msg)
            else:
                missing_field_msg = (
                    f'{i} missing from JSON provided by Kuali '
                    f'for {self.duid.value}'
                )
                base_log.info(missing_field_msg)

            if value_list:
                # First, make sure "other" is at the end.
                value_list.sort(key=(self.OTHER).__eq__)

                # Then, build up the value string.
                for item in value_list:
                    value += f'{item}, '

            if value:
                try:
                    # Trim any terminating comma
                    if (value[-2] == ','):
                        value = value[:-2]
                except Exception:
                    no_comma_msg = f'No terminating comma for {i}.value'
                    base_log.info(no_comma_msg)
            field.value = value

        for i in self.other_field_list:
            setattr(self, i, HealthDataField(i))
            field = getattr(self, i)
            json_entry = source_json.get(i)
            if json_entry:
                field.value = json_entry
            else:
                missing_field_msg = (
                    f'{i} missing from JSON provided by Kuali '
                    f'for {self.duid.value}'
                )
                base_log.info(missing_field_msg)

        try:
            self.completed = self.__check_completed()
        except Exception:
            completed_failsafe_msg = (
                'Required fields for computing completion status '
                'were missing from the JSON provided by Kuali! '
                'Defaulting to marking as not completed.'
            )
            base_log.warning(completed_failsafe_msg)
            self.completed = False

    def __check_completed(self):
        if not ((self.health_status_form_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.health_status_form_status.value.lower() ==
                 self.PREVIOUSLY_SUBMITTED)):
            return False

        if not ((self.OSHA_respirator_form_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.OSHA_respirator_form_status.value.lower() ==
                 self.PREVIOUSLY_SUBMITTED)
                or
                (self.OSHA_respirator_form_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.drug_screen_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.drug_screen_status.value.lower() ==
                 self.PREVIOUSLY_SUBMITTED)):
            return False

        if not ((self.color_vision_screening_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.color_vision_screening_status.value.lower() ==
                 self.PREVIOUSLY_SUBMITTED)
                or
                (self.color_vision_screening_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.measles_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.measles_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.mumps_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.mumps_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.rubella_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.rubella_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.varicella_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.varicella_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.tdap_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.tdap_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.flu_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.flu_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.covid_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.covid_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.tb_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.tb_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.hepb_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.hepb_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        if not ((self.polio_immunity_status.value.lower() ==
                 self.COMPLETE)
                or
                (self.polio_immunity_status.value.lower() ==
                 self.NOT_REQUIRED)):
            return False

        return True


def generate_incomplete_form_email_blocks(healthData):
    text_prefix = '  * '
    html_prefix = '  &bull; '
    text_body_incomplete_form_components = (
        'Health Recommendation Form: '
        f'{healthData.health_status_form_status.value}\n'
        'OSHA Questionnaire for Respirator Users Form: '
        f'{healthData.OSHA_respirator_form_status.value}\n'
        'Color Vision Screening Form: '
        f'{healthData.color_vision_screening_status.value}\n'
        'Drug Screen: '
        f'{healthData.drug_screen_status.value}\n\n'
    )
    html_body_incomplete_form_components = (
        '<table>'
        '<tr><td>Health Recommendation Form:  </td>'
        f'<td>{healthData.health_status_form_status.value}</td></tr>'
        '<tr><td>OSHA Questionnaire for Respirator Users Form:  </td>'
        f'<td>{healthData.OSHA_respirator_form_status.value}</td></tr>'
        '<tr><td>Color Vision Screening Form:  </td>'
        f'<td>{healthData.color_vision_screening_status.value}</td></tr>'
        '<tr><td>Drug Screen:  </td>'
        f'<td>{healthData.drug_screen_status.value}</td></tr>'
        '</table>'
        '<br/>'
    )

    # Measles: [measles status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Measles: '
        f'{healthData.measles_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Measles: '
        f'{healthData.measles_immunity_status.value}'
        f'</td></tr>'
    )
    # [measles next steps]
    if (
            healthData.measles_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.measles_incomplete_reason.value
        other = healthData.measles_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [measles doc status]
        for i in range(1, 4+1):
            accepted = getattr(healthData, f'MMR_doc{i}_accepted')
            reject_reason = getattr(healthData, f'MMR_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'mmr_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'mmr_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}MMR vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'MMR vaccine dose {i} - {reason}'
                    f'</td><tr>'
                )

        for i in range(1, 2+1):
            accepted = getattr(healthData,
                               f'measles_doc{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'measles_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'measles_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'measles_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Measles vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Measles vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        if (
                healthData.measles_titer_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.measles_titer_reject_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.measles_titer_missing_info
                missing_other = healthData.measles_titer_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Measles titer - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Measles titer - {reason}'
                f'</td></tr>'
            )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Mumps: [mumps status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Mumps: '
        f'{healthData.mumps_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Mumps: '
        f'{healthData.mumps_immunity_status.value}'
        f'</td></tr>'
    )
    # [mumps next steps]
    if (
            healthData.mumps_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.mumps_incomplete_reason.value
        other = healthData.mumps_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [mumps doc status]
        for i in range(1, 4+1):
            accepted = getattr(healthData, f'MMR_doc{i}_accepted')
            reject_reason = getattr(healthData, f'MMR_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'mmr_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'mmr_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}MMR vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'MMR vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        for i in range(1, 2+1):
            accepted = getattr(healthData, f'mumps_doc{i}_accepted')
            reject_reason = getattr(healthData, f'mumps_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'mumps_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'mumps_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Mumps vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Mumps vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        if (
                healthData.mumps_titer_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.mumps_titer_reject_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.mumps_titer_missing_info
                missing_other = healthData.mumps_titer_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Mumps titer - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Mumps titer - {reason}'
                f'</td></tr>'
            )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Rubella: [rubella status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Rubella: '
        f'{healthData.rubella_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Rubella: '
        f'{healthData.rubella_immunity_status.value}'
        f'</td></tr>'
    )
    # [rubella next steps]
    if (
            healthData.rubella_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.rubella_incomplete_reason.value
        other = healthData.rubella_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [rubella doc status]
        for i in range(1, 4+1):
            accepted = getattr(healthData, f'MMR_doc{i}_accepted')
            reject_reason = getattr(healthData, f'MMR_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'mmr_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'mmr_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}MMR vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'MMR vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        for i in range(1, 1+1):
            accepted = getattr(healthData,
                               f'rubella_doc{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'rubella_doc{i}_reject_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'rubella_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'rubella_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Rubella vaccine - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Rubella vaccine - {reason}'
                    f'</td></tr>'
                )

        if (
                healthData.rubella_titer_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.rubella_titer_reject_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.rubella_titer_missing_info
                missing_other = healthData.rubella_titer_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Rubella titer - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Rubella titer - {reason}'
                f'</td></tr>'
            )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Varicella: [varicella status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Varicella: '
        f'{healthData.varicella_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Varicella: '
        f'{healthData.varicella_immunity_status.value}'
        f'</td></tr>'
    )
    # [varicella next steps]
    if (
            healthData.varicella_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.varicella_incomplete_reason.value
        other = healthData.varicella_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [varicella doc status]
        for i in range(1, 3+1):
            accepted = getattr(healthData, f'varicella_doc{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'varicella_doc{i}_rejected_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'varicella_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'varicella_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Varicella vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Varicella vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        if (
                healthData.varicella_titer_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.varicella_titer_rejected_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.varicella_titer_missing_info
                missing_other = healthData.varicella_titer_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Varicella titer - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Varicella titer - {reason}'
                f'</td></tr>'
            )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Tdap: [tdap status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Tdap: '
        f'{healthData.tdap_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Tdap: '
        f'{healthData.tdap_immunity_status.value}'
        f'</td></tr>'
    )
    # [tdap next steps]
    if (
            healthData.tdap_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.tdap_incomplete_reason.value
        other = healthData.tdap_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [tdap doc status]
        for i in range(1, 1+1):
            accepted = healthData.tdap_doc_accepted
            reject_reason = healthData.tdap_doc_rejected_reason
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = healthData.tdap_doc_missing_info
                    missing_other = healthData.tdap_doc_missing_other
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Tdap vaccine - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Tdap vaccine - {reason}'
                    f'</td></tr>'
                )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Influenza: [flu status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Influenza: '
        f'{healthData.flu_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Influenza: '
        f'{healthData.flu_immunity_status.value}'
        f'</td></tr>'
    )
    # [flu next steps]
    if (
            healthData.flu_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.flu_incomplete_reason.value
        other = healthData.flu_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [flu doc status]
        for i in range(1, 1+1):
            accepted = healthData.flu_doc_accepted
            reject_reason = healthData.flu_doc_rejected_reason
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = healthData.flu_doc_missing_info
                    missing_other = healthData.flu_doc_missing_other
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Influenza vaccine - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Influenza vaccine - {reason}'
                    f'</td></tr>'
                )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # COVID-19: [covid status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'COVID-19: '
        f'{healthData.covid_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'COVID-19: '
        f'{healthData.covid_immunity_status.value}'
        f'</td></tr>'
    )
    # [covid next steps]
    if (
            healthData.covid_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.covid_incomplete_reason.value
        other = healthData.covid_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [covid doc status]
        for i in range(1, 7+1):
            accepted = getattr(healthData,
                               f'covid_doc{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'covid_doc{i}_rejected_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'covid_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'covid_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}COVID-19 vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'COVID-19 vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Tuberculosis: [tb status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Tuberculosis: '
        f'{healthData.tb_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Tuberculosis: '
        f'{healthData.tb_status.value}'
        f'</td></tr>'
    )
    # [tb next steps]
    if (
            healthData.tb_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.tb_next_steps.value
        other = healthData.tb_next_steps_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [tb doc status]
        for i in range(1, 1+1):
            accepted = healthData.tb_pos_test_doc_accepted
            reject_reason = healthData.tb_pos_test_doc_rejected_reason
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = healthData.tb_pos_test_doc_missing_info
                    missing_other = healthData.tb_pos_test_doc_missing_other
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}TB Positive Test - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'TB Positive Test - {reason}'
                    f'</td></tr>'
                )

        for i in range(1, 1+1):
            accepted = healthData.tb_neg_skin_doc_accepted
            reject_reason = healthData.tb_neg_skin_doc_rejected_reason
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = healthData.tb_neg_skin_doc_missing_info
                    missing_other = healthData.tb_neg_skin_doc_missing_other
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Negative TB Skin Test - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Negative TB Skin Test - {reason}'
                    f'</td></tr>'
                )

        for i in range(1, 1+1):
            accepted = healthData.tb_neg_blood_doc_accepted
            reject_reason = healthData.tb_neg_blood_doc_rejected_reason
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = healthData.tb_neg_blood_doc_missing_info
                    missing_other = healthData.tb_neg_blood_doc_missing_other
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Negative TB Blood Test - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Negative TB Blood Test - {reason}'
                    f'</td></tr>'
                )

        for i in range(1, 2+1):
            accepted = getattr(healthData,
                               f'chest_xray_{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'chest_xray_{i}_rejected_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'chest_xray_{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'chest_xray_{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Chest X-Ray {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Chest X-Ray {i} - {reason}'
                    f'</td></tr>'
                )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Hepatitis B: [hepb status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Hepatitis B: '
        f'{healthData.hepb_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Hepatitis B: '
        f'{healthData.hepb_immunity_status.value}'
        f'</td></tr>'
    )
    # [hepb next steps]
    if (
            healthData.hepb_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.hepb_incomplete_reason.value
        other = healthData.hepb_incomplete_reason_other.value
        if reason.lower() == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [hepb doc status]
        for i in range(1, 6+1):
            accepted = getattr(healthData, f'hepb_doc{i}_accepted')
            reject_reason = getattr(healthData, f'hepb_doc{i}_rejected_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'hepb_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'hepb_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Hepatitis B vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Hepatitis B vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )

        if (
                healthData.hepb_titer_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.hepb_titer_rejected_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.hepb_titer_missing_info
                missing_other = healthData.hepb_titer_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Hepatitis B titer - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Hepatitis B titer - {reason}'
                f'</td></tr>'
            )

        if (
                healthData.hepb_antigen_accepted.value.lower() ==
                healthData.NO
        ):
            reason = healthData.hepb_antigen_rejected_reason.value
            if reason.lower() == healthData.DOC_MISSING:
                missing_info = healthData.hepb_antigen_missing_info
                missing_other = healthData.hepb_antigen_missing_other
                reason = f'{reason}: {missing_info.value}'
                if missing_other.value:
                    reason = f'{reason} - {missing_other.value}'
            text_body_incomplete_form_components = (
                f'{text_body_incomplete_form_components}'
                f'{text_prefix}Hepatitis B Antigen - {reason}\n'
            )
            html_body_incomplete_form_components = (
                f'{html_body_incomplete_form_components}'
                f'<tr><td>{html_prefix}'
                f'Hepatitis B Antigen - {reason}'
                f'</td></tr>'
            )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}\n'
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        f'<br/>'
    )

    # Polio: [polio status]
    text_body_incomplete_form_components = (
        f'{text_body_incomplete_form_components}'
        'Polio: '
        f'{healthData.polio_immunity_status.value}\n'
    )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'<table>'
        f'<tr><td>'
        'Polio: '
        f'{healthData.polio_immunity_status.value}'
        f'</td></tr>'
    )
    # [polio next steps]
    if (
            healthData.polio_immunity_status.value.lower() ==
            healthData.INCOMPLETE
    ):
        reason = healthData.polio_incomplete_reason.value
        other = healthData.polio_incomplete_reason_other.value
        if reason == healthData.OTHER:
            reason = other
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            f'Reason: {reason}\n'
        )
        html_body_incomplete_form_components = (
            f'{html_body_incomplete_form_components}'
            f'<tr><td>'
            f'Reason: {reason}'
            f'</td></tr>'
        )
        # [polio doc status]
        for i in range(1, 5+1):
            accepted = getattr(healthData,
                               f'polio_doc{i}_accepted')
            reject_reason = getattr(healthData,
                                    f'polio_doc{i}_rejected_reason')
            if accepted.value.lower() == healthData.NO:
                reason = reject_reason.value
                if reason.lower() == healthData.DOC_MISSING:
                    missing_info = getattr(healthData,
                                           f'polio_doc{i}_missing_info')
                    missing_other = getattr(healthData,
                                            f'polio_doc{i}_missing_other')
                    reason = f'{reason}: {missing_info.value}'
                    if missing_other.value:
                        reason = f'{reason} - {missing_other.value}'
                text_body_incomplete_form_components = (
                    f'{text_body_incomplete_form_components}'
                    f'{text_prefix}Polio vaccine dose {i} - {reason}\n'
                )
                html_body_incomplete_form_components = (
                    f'{html_body_incomplete_form_components}'
                    f'<tr><td>{html_prefix}'
                    f'Polio vaccine dose {i} - {reason}'
                    f'</td></tr>'
                )
        text_body_incomplete_form_components = (
            f'{text_body_incomplete_form_components}'
            # No final newline; will be added by surrounding email.
        )
    html_body_incomplete_form_components = (
        f'{html_body_incomplete_form_components}'
        f'</table>'
        # Last table; not adding a line break here.
    )

    return (
        text_body_incomplete_form_components,
        html_body_incomplete_form_components
    )


def mail_response(healthData):
    text_body_incomplete_form_components = None
    html_body_incomplete_form_components = None
    try:
        if not healthData.completed:
            (
                text_body_incomplete_form_components,
                html_body_incomplete_form_components
            ) = generate_incomplete_form_email_blocks(healthData)
    except Exception as e:
        print(f'Encountered a problem generating '
              f'the incomplete email form components: '
              f'{str(e)}')
        print('Exception backtrace follows:')
        print(format_exc())

    text_body_incomplete = (
        f'Dear {healthData.displayName.value},\n\n'
        f'Our team has reviewed your submission.\n'
        f'PLEASE NOTE, you have not submitted all '
        f'of the required documentation.\n'
        f'Read each item below to see what is still '
        f'needed before you can be medically cleared.\n\n'
        f'{text_body_incomplete_form_components}'
        f'\n'
        f'Your prompt attention to any outstanding '
        f'documentation is necessary to ensure you '
        f'can be medically cleared to start work on time.\n\n'
        f'If you have any questions about next steps '
        f'regarding medical documentation or medical '
        f'requirements, please contact us at 919-684-3136 '
        f'option 2, email eohremote@dm.duke.edu.\n\n'
        f'Thank you,\n\n'
        f'Duke Employee Occupational Health and Wellness\n'
    )

    html_body_incomplete = (
        f'<html><head></head><body><p>'
        f'Dear {healthData.displayName.value},<br/><br/>'
        f'Our team has reviewed your submission. '
        f'<div style="color:red">'
        f'PLEASE NOTE, you have not submitted all '
        f'of the required documentation. '
        f'</div>'
        f'Read each item below to see what is still '
        f'needed before you can be medically cleared.<br/></p>'
        f'<p>'
        f'{html_body_incomplete_form_components}'
        f'</p><p>'
        f'Your prompt attention to any outstanding '
        f'documentation is necessary to ensure you '
        f'can be medically cleared to start work on time.<br/><br/>'
        f'If you have any questions about next steps '
        f'regarding medical documentation or medical '
        f'requirements, please contact us at 919-684-3136 '
        f'option 2, email <a href="mailto:eohremote@dm.duke.edu">'
        f'eohremote@dm.duke.edu</a>.<br/><br/>'
        f'Thank you,<br/><br/>'
        f'Duke Employee Occupational Health and Wellness</p>'
        f'</body></html>'
    )

    text_body_completed = (
        f'Dear {healthData.displayName.value},\n\n'
        f'Thank you for submitting your medical '
        f'documentation to Duke Employee Occupational '
        f'Health and Wellness (EOHW). '
        f'Your documentation has been reviewed, '
        f'and you will hear about your '
        f'clearance status shortly.\n\n'
        f'Thank you,\n\n'
        f'Duke Employee Occupational Health and Wellness\n'
    )

    html_body_completed = (
        f'<html><head></head><body><p>'
        f'Dear {healthData.displayName.value},<br/><br/>'
        f'Thank you for submitting your medical '
        f'documentation to Duke Employee Occupational '
        f'Health and Wellness (EOHW). '
        f'Your documentation has been reviewed, '
        f'and you will hear about your '
        f'clearance status shortly.<br/><br/>'
        f'Thank you,<br/><br/>'
        f'Duke Employee Occupational Health and Wellness</p>'
        f'</body></html>'
    )

    text_body = text_body_incomplete
    html_body = html_body_incomplete
    subject = 'ACTION REQUIRED: Duke EOHW needs additional information'
    if healthData.completed:
        text_body = text_body_completed
        html_body = html_body_completed
        subject = 'Duke EOHW medical clearance status update'

    message = MIMEMultipart()
    message['From'] = smtp_sender
    message['To'] = healthData.email.value
    message['Subject'] = subject

    body = MIMEMultipart('alternative')
    text_part = MIMEText(text_body, 'plain', 'utf-8')
    html_part = MIMEText(html_body, 'html', 'utf-8')
    body.attach(text_part)
    body.attach(html_part)
    message.attach(body)

    if not healthData.completed:
        pdf_part = MIMEBase('application', 'pdf')
        pdf_part.add_header('Content-Transfer-Encoding', 'base64')
        encoded_pdf = b64encode(reqs_file)
        pdf_part.set_payload(encoded_pdf, 'utf-8')
        pdf_part_content_disposition = (
            'attachment; filename='
            '"Health Documentation Requirements Table.pdf"'
        )
        pdf_part.add_header('Content-Disposition',
                            pdf_part_content_disposition)
        message.attach(pdf_part)

    text = message.as_string()

    fail_msg = (
        f'Summary email send failed to {healthData.email.value}'
    )

    ssl_context = ssl_create_context()
    smtp_retry_count = smtp_retry_global_count
    server = None
    while smtp_retry_count > 0:
        try:
            server = SMTP(smtp_server, smtp_port)
        except Exception as e:
            smtp_log.warning((f'Exception occurred during SMTP setup: '
                              f'{type(e)}'))
            server = None
            continue

        if server:
            try:
                server.starttls(context=ssl_context)
                server.sendmail(smtp_sender, healthData.email.value, text)

                success_msg = (
                    f'Summary email sent to '
                    f'{healthData.email.value}'
                )
                smtp_log.info(success_msg)
                smtp_retry_count = 0
            except Exception as e:
                smtp_log.warning(fail_msg)
                smtp_log.warning((f'Exception resulting in failure to send: '
                                  f'{type(e)}'))
                smtp_retry_count -= 1
                smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
                continue
            finally:
                server.quit()
        else:
            smtp_log.warning(fail_msg)
            smtp_retry_count -= 1
            smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
            sleep(smtp_retry_global_sleep)
            continue


# SQLAlchemy boilerplate
Base = declarative_base()

# Database access parameters
pg_username = environ.get("PG_USERNAME")
pg_password = environ.get("PG_PASSWORD")
pg_hostname = environ.get("PG_HOSTNAME")
pg_port = environ.get("PG_PORT")
pg_db = environ.get("PG_DATABASE")

pg_connection_string_fmt = (
    'postgresql+pg8000://{username}:{password}@' +
    '{hostname}:{port}/{database}'
)
pg_url = pg_connection_string_fmt.format(
    username=pg_username, password=pg_password,
    hostname=pg_hostname, port=pg_port,
    database=pg_db
)

# Create SqlAlchemy engine for Postgres, with SSL,
# and a sessionmaker
pg_ssl_context = ssl_create_context()
pg_engine = sa_create_engine(pg_url,
                             client_encoding='utf8',
                             connect_args={"ssl_context": pg_ssl_context},
                             pool_size=10,
                             max_overflow=0,
                             pool_recycle=600,
                             pool_pre_ping=True,
                             pool_use_lifo=True,
                             echo=False)
pg_Session = sessionmaker(bind=pg_engine)


# Define the tables we'll be using in Postgres to record what
# comes from Kuali Build.
class NewHireDoc(Base):
    __tablename__ = 'new_hire_doc'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    duid = Column(String(15), index=True)
    txn_id = Column(UUID(as_uuid=True))
    txn_date = Column(DateTime)
    completed = Column(Boolean)
    source_json = Column(JSONB)

    def __repr__(self):
        return ("<NewHireDoc(tbl_id='%s', " +
                "duid='%s', txn_id='%s', txn_date='%s', completed='%s')>") % (
                    self.tbl_id, self.duid, self.txn_id,
                    self.txn_date, self.completed)


# Set up tables, if not already present.
while True:
    try:
        Base.metadata.create_all(pg_engine)
    except IntegrityError:
        continue
    except SystemExit:
        print('System exit forced while in table setup loop!')
        print('Investigate database connectivity.')
        break
    except Exception:
        print('An unexpected error occurred while trying to set up tables!')
        raise
    else:
        break

# Body of New Hire Health Data app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = (
    '/opt/new_hire_doc_service/credentials/htpasswd'
)
app.config['FLASK_AUTH_ALL'] = True
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)


def put_pg_db_data(healthData, source_json):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            txn_id = uuid4()
            txn_date = datetime.now()

            surveillance_record = NewHireDoc(duid=healthData.duid.value,
                                             txn_id=txn_id,
                                             txn_date=txn_date,
                                             completed=healthData.completed,
                                             source_json=source_json)
            session.add(surveillance_record)
            session.commit()

            retval = True
        else:
            print('Unable to get connection to backend Postgres DB.')
    except Exception as e:
        print(f'Encountered a problem communicating '
              f'with backend Postgres DB: '
              f'{str(e)}')
        print('Exception backtrace follows:')
        print(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            print(f'Encountered a problem performing rollback '
                  f'on backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            print(f'Encountered a problem closing connection '
                  f'with backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    return retval


@app.route('/api/v1/new_hire_doc_submit', methods=['POST'])
def submitted_new_hire_doc():
    if request.method != 'POST':
        return make_response('Malformed request.\n', 400)

    source_json = request.json
    healthData = HealthData(source_json)

    insert_success = False
    if healthData.duid.value:
        insert_success = put_pg_db_data(healthData, source_json)

    if insert_success:
        if healthData.email.value:
            # Queue sending the response, if an email address was provided.
            mail_queue.put(healthData)
        else:
            # Log that the report was not sent.
            no_email_msg = (
                f'{healthData.duid.value} did not provide an email address; '
                f'response not sent.'
            )
            smtp_log.warning(no_email_msg)

        return make_response('Update accepted.\n', 200)

    return make_response('Malformed data.\n', 400)


@app.route('/api/v1/new_hire_doc_online', methods=['GET'])
def online():
    if request.method != 'GET':
        return make_response('Malformed request.\n', 400)

    current_dt_str = str(datetime.utcnow().isoformat('#'))
    response_str = f'Online - {current_dt_str}\n'

    return make_response(response_str, 200)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
