itsdangerous == 2.0.1
flask-htpasswd >= 0.4.0
gunicorn >= 19.9.0
gevent >= 1.4
setproctitle >= 1.2.2
SQLAlchemy >= 1.4.31
pg8000 >= 1.24.0
pytz >= 2021.1
